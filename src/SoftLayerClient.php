<?php

namespace Drupal\softlayer;

use Exception;
use SoftLayer\SoapClient;

/**
 * Provides methods for calling the SoftLayer API.
 */
class SoftLayerClient {

  /**
   * Username to authenticate against SOAP server.
   *
   * @var string
   */
  protected $auth_username;

  /**
   * API key to authenticate against SOAP server.
   *
   * @var string
   */
  protected $auth_key;

  /**
   * SOAP server url.
   *
   * @var string
   */
  protected $api_url;

  /**
   * Service name to interact with using the SOAP server.
   *
   * @var string
   */
  protected $service;

  /**
   * Sets up new SoftLayerClient object.
   *
   * @param $config
   *   The config factory argument.
   */
  public function __construct($config) {
    $this->auth_username = $config->get('softlayer.settings')->get('softlayer_auth_username');
    $this->auth_key = $config->get('softlayer.settings')->get('softlayer_auth_key');
    $this->api_url = $config->get('softlayer.settings')->get('softlayer_api_url');
  }

  /**
   * Sets the name of the service we want to interact with from SOAP server.
   *
   * @param string $service
   *   Service name from SOAP server to be used for this client.
   * @param integer $id
   *   Unique identifier of the object to initialize the client.
   *
   * @return SoftLayer\SoapClient
   */
  public function service($service, $id = NULL) {
    $this->service = $service;
    return SoapClient::getClient(
      $this->service,
      $id,
      $this->auth_username,
      $this->auth_key,
      $this->api_url
    );
  }

  /**
   * Getter for service name.
   *
   * @return string
   */
  public function getService() {
    return $this->service;
  }

  /**
   * Returns the connection status of the API.
   *
   * @return bool
   *   Returns TRUE or FALSE whether or not the API is successfully connecting.
   */
  public function connected() {
    try {
      $this->service('SoftLayer_Utility_Network')->whois('drupal.org');
      return TRUE;
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

}
