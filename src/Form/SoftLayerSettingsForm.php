<?php

namespace Drupal\softlayer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SoftLayer API configuration settings form.
 */
class SoftLayerSettingsForm extends ConfigFormBase {

  private const SLAPI_PUBLIC_URL = 'https://api.softlayer.com/soap/v3/';
  private const SLAPI_PRIVATE_URL = 'http://api.service.softlayer.com/soap/v3/';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'softlayer_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['softlayer.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('softlayer.settings');

    $form['softlayer_api_url'] = [
      '#type' => 'select',
      '#title' => $this->t('URL'),
      '#required' => TRUE,
      '#default_value' => $config->get('softlayer_api_url') != self::SLAPI_PUBLIC_URL && $config->get('softlayer_api_url') != self::SLAPI_PRIVATE_URL ? 'other' : $config->get('softlayer_api_url'),
      '#options' => [
        self::SLAPI_PUBLIC_URL => $this->t('Public'),
        self::SLAPI_PRIVATE_URL => $this->t('Private'),
        'other' => $this->t('Other'),
      ],
    ];

    $form['softlayer_api_url_other'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Other'),
      '#default_value' => $config->get('softlayer_api_url') != self::SLAPI_PUBLIC_URL && $config->get('softlayer_api_url') != self::SLAPI_PRIVATE_URL ? $config->get('softlayer_api_url') : 'http://',
      '#states' => [
        'visible' => [
          ':input[name="softlayer_api_url"]' => ['value' => 'other'],
        ],
        'required' => [
          ':input[name="softlayer_api_url"]' => ['value' => 'other'],
        ],
      ],
    ];

    $form['softlayer_authentication'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Authentication'),
    ];

    $form['softlayer_authentication']['softlayer_auth_username'] = [
      '#default_value' => $config->get('softlayer_auth_username'),
      '#required' => TRUE,
      '#title' => $this->t('Username'),
      '#type' => 'textfield',
    ];

    $form['softlayer_authentication']['softlayer_auth_key'] = [
      '#default_value' => $config->get('softlayer_auth_key'),
      '#required' => TRUE,
      '#title' => $this->t('API Key'),
      '#type' => 'textfield',
    ];

    if ($response = \Drupal::service('softlayer.client')->connected()) {
      drupal_set_message($this->t('SoftLayer API successfully connected.'));
    }
    else {
      drupal_set_message($this->t('Unable to connect to SoftLayer API.'), 'notice');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('softlayer.settings');
    $config->set('softlayer_api_url', $form_state->getValue('softlayer_api_url') == 'other' ? $form_state->getValue('softlayer_api_url_other') : $form_state->getValue('softlayer_api_url'));
    $config->set('softlayer_auth_username', $form_state->getValue('softlayer_auth_username'));
    $config->set('softlayer_auth_key', $form_state->getValue('softlayer_auth_key'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
