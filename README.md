SoftLayer
------

### Summary

Provides basic functionality to allow other modules to connect and make calls to
SoftLayer API services.

### Configuration

Using Composer

1. On your project root directory, run `composer require softlayer/softlayer-api-php-client`

Without Composer

1. Download and unzip the [SoftLayer API PHP client](https://github.com/softlayer/softlayer-api-php-client) inside your project libraries directory.
2. Enable the SoftLayer module and give user roles access to the "Administer SoftLayer API" permission.
3. Go to the module settings page at [Configuration > Web services > SoftLayer API](/admin/config/services/softlayer)
4. Set the API URL, in most cases this will either be "Public" or "Private".
5. Set credentials to connect to API endpoint
6. Save the form.

### Usage

Use the **softlayer.client** service to make calls to the API.

_Note: More extensive documentation of the services and methods available with the SoftLayer API can be found on the [SoftLayer Developer Network](http://sldn.softlayer.com)._

#### Basic usage

```
\Drupal::service('softlayer.client')->service($service)->{$methodName}($parameters);
```

**Example:**

```
\Drupal::service('softlayer.client')->service('SoftLayer_Utility_Network')->whois('drupal.org');
```

#### Advanced usage

```
$client = \Drupal::service('softlayer.client')->service('SoftLayer_Ticket', 12345);

$objectMask = new ObjectMask();
$objectMask->updates;
$objectMask->assignedUser;
$objectMask->attachedHardware->datacenter;

$client->setObjectMask($objectMask);

try {
  $ticket = $client->getObject();
  print_r($ticket);
} catch (\Exception $e) {
  die('Unable to retrieve ticket record: ' . $e->getMessage());
}
```

For more advanced usage with _limitResults_, _objectFilter_ and _objectMask_ options, visit the [SoftLayer PHP library](https://github.com/softlayer/softlayer-api-php-client) on GitHub.

### Maintainers

- [Ian Whitcomb](https://www.drupal.org/u/iwhitcomb)
- [Zequi Vázquez](https://www.drupal.org/u/rabbitlair)
